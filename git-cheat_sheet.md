# MATLAB
### Cheatsheat by Julian Dietzel

## Stage

| command | explanation |       
| --- | --- |
| ```add --all``` | add all changes (additions / removals / updates) |
| ```add <...> -n``` / ```add <...> --dry-run``` | print files that would be staged but does not stage them |
| ```reset``` | unstages all files |
| ```diff``` / ```diff --name-only```| show all changes / files that are not staged |
| ```diff --cached``` / ```diff --cached --name-only```| show all staged changes / files |

## Commit & Push

| command | explanation |       
| --- | --- |
| ```commit -m "..."``` |commit all staged files with commit-message in brackets|
| ```push origin branch``` |push commits onto set branch|

## Branching

| command | explanation |       
| --- | --- |
| ```branch branchname``` |create new branch with branchname|
| ```checkout branchname``` |set HEAD to branch|
| ```checkout -b branchname```|create new branch with branchname and set HEAD to branch|
